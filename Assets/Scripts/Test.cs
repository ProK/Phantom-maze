﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    float speed;
    Rigidbody2D rb;
    Animator anim;
    bool isFacingRight;


    // Start is called before the first frame update
    void Start()
    {
        speed = 3;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        isFacingRight = true;
    }

    // Update is called once per frame
    void Update()
    {
        float movementX = Input.GetAxis("Horizontal");
        float movementY = Input.GetAxis("Vertical");

        anim.SetBool("Movement", isMoving(movementX, movementY));

        rb.velocity = new Vector2(movementX * speed, movementY * speed);

        if (movementX > 0 && !isFacingRight)
        {
            FlipSprite();
        }
        else if(movementX < 0 && isFacingRight){
            FlipSprite();
        }
    }

    void FlipSprite() {
        isFacingRight = !isFacingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

    bool isMoving(float moveX, float moveY) {      
        return  Mathf.Abs(moveX) > 0 || Mathf.Abs(moveY) > 0 ? true : false;
    }
}
