﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    enum States { PATROL, CHASE }; // The number of availabe states
    [SerializeField]
    States enemyState;
    bool isFacingRight;

    [SerializeField]
    Transform[] patrolPoints;
    [SerializeField]
    Transform player;
    [SerializeField]
    int currentPoint;
    float speed;

    // Start is called before the first frame update
    void Awake()
    {
        enemyState = States.PATROL;
        currentPoint = 0;
        isFacingRight = true;
        speed = 1.5f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        DoActionInState(enemyState);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // When the player enters inside the circle collider, the enemy starts hunting
        if (collision.tag.Equals("Player")) {
            enemyState = States.CHASE;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        // When the player exits the circle, the enemy returns to patrol
        if(collision.tag.Equals("Player"))
            enemyState = States.PATROL;
    }

    void DoActionInState(States state) {

        if (state == States.PATROL)
        {
            Patrol();
        }
        else if (state == States.CHASE)
        {
            Chase();
        }
    }

    void Patrol() {
        // Calculate the distance to the next patrolling point
        float distanceToPoint = Vector2.Distance((Vector2) this.transform.position,
            patrolPoints[currentPoint].position);

        // Then, if we are next to a waypoint
        if (distanceToPoint < 0.3f)
        {
            // We change to the next point
            currentPoint = (currentPoint + 1) % patrolPoints.Length;
        }
        else {
            // Gets the point position
            Vector2 newPos = Vector2.MoveTowards(this.transform.position,
                                                    patrolPoints[currentPoint].position,
                                                    speed * Time.deltaTime);

            // Moves to the next point
            GetComponent<Rigidbody2D>().MovePosition(newPos);
        }

        // Calculate the direction that the enemy will face
        Vector2 newDirection = patrolPoints[currentPoint].position - this.transform.position;

        // And finally we flip the sprite if needed
        if (newDirection.x > 0 && !isFacingRight)
        {
            FlipSprite();
        }
        else if (newDirection.x < 0 && isFacingRight)
        {
            FlipSprite();
        }

    }
    void Chase() {

        // Calculate the direction that the enemy will face
        Vector2 newDirection = player.transform.position - this.transform.position;

        // And finally we flip the sprite if needed
        if (newDirection.x > 0 && !isFacingRight)
        {
            FlipSprite();
        }
        else if (newDirection.x < 0 && isFacingRight)
        {
            FlipSprite();
        }

        // Calculate direction to player
        Vector2 direction = Vector2.MoveTowards(this.transform.position,
                                                player.position,
                                                speed * Time.deltaTime);
        // Moves to player;
        GetComponent<Rigidbody2D>().MovePosition(direction);
    }

    void FlipSprite()
    {
        isFacingRight = !isFacingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

}
